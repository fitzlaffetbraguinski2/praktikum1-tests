include (gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += thread

SOURCES += main.cpp \
    logger.cpp graphic.cpp \
    game.cpp test/fake/fakegraphic.cpp \
    piece.cpp

HEADERS += piece.h connectfour.h \
    logger.h graphic.h \
    game.h \
    tst_gamelogic.h \
    mockgraphic.h \
    igraphic.h test/tst_gamelogic.h test/mock/mockgraphic.h \
    test/fake/fakegraphic.h

LIBS += -lncurses
