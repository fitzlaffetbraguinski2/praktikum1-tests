# Praktikum 1 Tests

Diese Tests sind nicht offiziell oder von irgendjemandem gefordert.

Googletest kann mit `git submodule add https://github.com/google/googletest` als Submodule eingebunden werden. Die .pri (Project Include Datei) muss in die .pro Datei eingebunden werden.

Kompilierung erfolgt mit `qmake "DEFINES += PERFORM_TESTS" && make` oder man schreibt
die Präprozessorkonstante einfach in die main.cpp und benutzt irgendein anderes Build-Tool.

Die Objekte, die man mocken will (hier die Grafik) müssen von einer abstrakten Klasse erben genauso wie das Originalobjekt.
Der Spiellogik übergibt man dann einfach einen Zeiger auf das Objekt, egal ob Mock/Fake oder echt.

