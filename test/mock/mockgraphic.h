#ifndef MOCKGRAPHIC_H
#define MOCKGRAPHIC_H

#include <gmock/gmock.h>
#include "../../igraphic.h"
#include "../fake/fakegraphic.h"

struct MockGraphic : public Igraphic{
  MockGraphic(string input) : input{input}, fake{fakegraphic{input}}{
      DelegateToFake();
  }
  MOCK_METHOD(void, initialize_field, (int scorex, int scorey), (override));
  MOCK_METHOD(void, put, (char player, int row, int col), (override));
  MOCK_METHOD(void, reset_field, (int scorex, int scorey), (override));
  MOCK_METHOD(void, win_message, (char player), (override));
  MOCK_METHOD(char, get_input, (), (override));
  MOCK_METHOD(void, change_active_player, (char player), (override));
  MOCK_METHOD(void, clear, (), (override));
  MOCK_METHOD(void, update_scoreboard, (char player, int score), (override));
  void DelegateToFake(){
      ON_CALL(*this, get_input).WillByDefault([this](){
          return fake.get_input();
      });
  }
private:
  string input;
  fakegraphic fake;
};

#endif // MOCKGRAPHIC_H
