#ifndef TST_GAMELOGIC_H
#define TST_GAMELOGIC_H

#include <algorithm>
#include <array>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <string>
#include <vector>

#include "game.h"
#include "logger.h"
#include "test/mock/mockgraphic.h"

using namespace testing;

using std::string;
using std::vector;

#define simpleSetup NiceMock<MockGraphic> mockgraphic{"q"};

#define run {\
    game game(&mockgraphic);\
    game.play();\
}

logging::Logger &logger = logging::Logger::getInstance("connectfour_log.txt", logLevels::TEST);

inline void log(string msg){
  logger.log(msg, logging::logLevels::TEST);
}

//https://stackoverflow.com/a/14266139/6455909
vector<string> split(string str, string delim){
    size_t pos = 0;
    vector<string> elements;
    string element;
    while ((pos = str.find(delim)) != string::npos) {
        element = str.substr(0, pos);
        elements.push_back(element);
        str.erase(0, pos + delim.length());
    }
    return elements;
}

string getOutputForLoggingTest(){
    string output = logger.output.str();
    int startIndex = output.find("Logging")+8;
    string moreRelevantOutput = output.substr(startIndex, output.size()-startIndex);
    vector<string> outputVector = split(moreRelevantOutput, "\n");
    std::transform(outputVector.begin(), outputVector.end(), outputVector.begin(),
                   [](string s)->string {return s.substr(22);});
    string usefulOutput;
    for(auto &el : outputVector) usefulOutput += (el + "\n");
    return usefulOutput;
}

TEST(GameTest, FieldGetsInitialized){
  log("Fields gets initialized");
  simpleSetup
  EXPECT_CALL(mockgraphic, initialize_field(_,_));
  run
}

TEST(GameTest, FieldGetsCleared){
  log("Field gets cleared");
  simpleSetup
  EXPECT_CALL(mockgraphic, clear());
  run
}

TEST(GameTest, InputIsCorrectlyRead){
  log("Input is correctly read");
  NiceMock<MockGraphic> mockgraphic{"1214q"}; //setup
  EXPECT_CALL(mockgraphic, get_input()).Times(5);
  EXPECT_CALL(mockgraphic, put('X', 5, 0));
  EXPECT_CALL(mockgraphic, put('O', 5, 1));
  EXPECT_CALL(mockgraphic, put('X', 4, 0));
  EXPECT_CALL(mockgraphic, put('O', 5, 3));
  run
}

TEST(GameTest, VerticalWin){
    log("Vertical win");
    NiceMock<MockGraphic> mockgraphic{"1213141q"};
    EXPECT_CALL(mockgraphic, get_input()).Times(8);
    EXPECT_CALL(mockgraphic, win_message('X'));
    run
}

TEST(GameTest, HorizontalWin){
    log("Horizontal win");
    NiceMock<MockGraphic> mockgraphic{"51527374n"};
    EXPECT_CALL(mockgraphic, get_input()).Times(9);
    EXPECT_CALL(mockgraphic, win_message('O'));
    run
}

TEST(GameTest, RightDiagonalWin){
    log("Right diagonal win");
    NiceMock<MockGraphic> mockgraphic{"321123123124k"};
    EXPECT_CALL(mockgraphic, get_input()).Times(13);
    EXPECT_CALL(mockgraphic, win_message('O'));
    run
}

TEST(GameTest, LeftDiagonalWin){
    log("Left diagonal win");
    NiceMock<MockGraphic> mockgraphic{"767577744566u"};
    EXPECT_CALL(mockgraphic, get_input()).Times(13);
    EXPECT_CALL(mockgraphic, win_message('O'));
    run
}

TEST(GameTest, Logging){
    log("Logging");
    NiceMock<MockGraphic> mockgraphic("22222229k~51314n");
    run
    string output = getOutputForLoggingTest();
    string expectedOutput =
R"((INFO) Player X has selected column 2
(INFO) Player O has selected column 2
(INFO) Player X has selected column 2
(INFO) Player O has selected column 2
(INFO) Player X has selected column 2
(INFO) Player O has selected column 2
(WARN) Player X has selected a full column (column 2)
(WARN) Invalid column: 9 by player X
(WARN) Invalid column: k by player X
(WARN) Invalid column: ~ by player X
(INFO) Player X has selected column 5
(INFO) Player O has selected column 1
(INFO) Player X has selected column 3
(INFO) Player O has selected column 1
(INFO) Player X has selected column 4
(INFO) Player X has won!
)";
    EXPECT_EQ(output, expectedOutput);
}

TEST(GameTest, CompleteGame){   //Vertical win
  log("Complete game");
  NiceMock<MockGraphic> mockgraphic("1121345671111796757b"); //setup
  EXPECT_CALL(mockgraphic, get_input()).Times(20);
  EXPECT_CALL(mockgraphic, change_active_player('X')).Times(8);
  EXPECT_CALL(mockgraphic, change_active_player('O')).Times(9);
  EXPECT_CALL(mockgraphic, put('X', 5, 0));
  EXPECT_CALL(mockgraphic, put('O', 4, 0));
  EXPECT_CALL(mockgraphic, put('X', 5, 1));
  EXPECT_CALL(mockgraphic, put('O', 3, 0));
  EXPECT_CALL(mockgraphic, put('X', 5, 2));
  EXPECT_CALL(mockgraphic, put('O', 5, 3));
  EXPECT_CALL(mockgraphic, put('X', 5, 4));
  EXPECT_CALL(mockgraphic, put('O', 5, 5));
  EXPECT_CALL(mockgraphic, put('X', 5, 6));
  EXPECT_CALL(mockgraphic, put('O', 2, 0));
  EXPECT_CALL(mockgraphic, put('X', 1, 0));
  EXPECT_CALL(mockgraphic, put('O', 0, 0));
  EXPECT_CALL(mockgraphic, put('X', 4, 6));
  EXPECT_CALL(mockgraphic, put('O', 4, 5));
  EXPECT_CALL(mockgraphic, put('X', 3, 6));
  EXPECT_CALL(mockgraphic, put('O', 4, 4));
  EXPECT_CALL(mockgraphic, put('X', 2, 6));
  EXPECT_CALL(mockgraphic, win_message('X'));
  run
}

TEST(GameTest, SecondRound){
 log("Second round");
 NiceMock<MockGraphic> mockgraphic{"1213141y51527374q"};
 EXPECT_CALL(mockgraphic, get_input()).Times(17);
 EXPECT_CALL(mockgraphic, win_message('X'));
 EXPECT_CALL(mockgraphic, win_message('O'));
 run
}


#endif // TST_GAMELOGIC_H
