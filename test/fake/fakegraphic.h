#include "../../igraphic.h"
#include <string>
using std::string;

class fakegraphic{
public:
    fakegraphic(string input);
    char get_input();
private:
    int index{0};
    string input;
};
