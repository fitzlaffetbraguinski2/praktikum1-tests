#include <gtest/gtest.h>
#include "game.h"
#include "test/tst_gamelogic.h"
using namespace std;

#ifndef PERFORM_TESTS
int main()
{
  // Our solution kappa
}
#else
int main(int argc, char *argv[])
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
#endif
